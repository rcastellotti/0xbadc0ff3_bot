import os
import requests
from flask import Flask, request, jsonify
from dotenv import load_dotenv
load_dotenv()

token = os.getenv("TOKEN")

url = f"https://api.telegram.org/bot{token}/sendMessage"
gitlab_url="https://gitlab.com/rcastellotti/0xbadc0ff3_bot/-/commit/"
fail="\U0000274c"
success="\U00002714"

app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False

@app.route("/", methods=["POST"])
def heroku_webhook():

    action = request.json["action"]
    version = request.json["data"]["release"]["version"]
    date = request.json["data"]["updated_at"]
    log = request.json["data"]["output_stream_url"]
    log = log.split("?",1)[0]
    status=request.json["data"]["status"]
    commit_sha=request.json["data"]["slug"]["commit"]
    commit_msg=request.json["data"]["slug"]["commit_description"]
    commit_msg=commit_msg.replace("  *","")
    commit_msg=commit_msg.replace(".","\.")

    if status=="succeeded":
        status=f"succeeded {success}"
    elif status=="failed":
        status=f"failed {fail}"
    
    if action == "update":
        message =f"""
*New version:* {version}
*Date:* `{date}`
*Status:* {status} 
*Build Log:* [log/v{version}]({log})
*Commit:*[{commit_msg}]({gitlab_url+commit_sha})"""
    message = {
        "chat_id": "@badc0ff3_status",
        "text": message,
        "parse_mode": "MarkdownV2",
        "disable_web_page_preview": True
    }
    r = requests.get(url, data=message)
    print(r.text)
    return jsonify({"telegram_update": "sent"})


if __name__ == "__main__":
    app.run(debug=True)
