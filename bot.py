import requests
import telegram
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import logging
import os
import yaml
from dotenv import load_dotenv

load_dotenv()

token = os.getenv("TOKEN")
url = "https://lichess.org/api/challenge/open"


def read_config():
    with open("config.yaml", "r") as config_file:
        config = yaml.safe_load(config_file)
    return config


def update_config(config):
    with open('config.yaml', 'w') as yamlfile:
        yaml.safe_dump(config, yamlfile)


def lichess_challenge_open():
    r = requests.post(url)
    json = r.json()
    return json["challenge"]["url"], json["urlWhite"], json["urlBlack"]


updater = Updater(token=token)
dispatcher = updater.dispatcher

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)


def bot_chess(update, context):
    creator_username = update.message.from_user.username
    group = update.message.chat.title
    logging.info(f"{creator_username} [{group}] created a challenge")
    chat_message = update.message.text[7:]

    url, urlWhite, urlBlack = lichess_challenge_open()

    if chat_message:
        if chat_message == "w":
            message = f"{creator_username} is white: {urlWhite}\nyou are black: {urlBlack}"
        if chat_message == "b":
            message = f"{creator_username} is black: {urlBlack}\nyou are white: {urlWhite}"
        context.bot.send_message(
            chat_id=update.effective_chat.id, text=message, disable_web_page_preview=True)
    else:
        message = f"let's play chess: {url}\nThe first person to come to this URL will play."
        context.bot.send_message(
            chat_id=update.effective_chat.id, text=message, disable_web_page_preview=True)


def bot_bigo(update, context):
    context.bot.send_message(chat_id=update.effective_chat.id,
                             text="https://www.bigocheatsheet.com/",
                             disable_web_page_preview=True)


def bot_commands(update, context):
    message = open("commands.md", mode="r").read()
    context.bot.send_message(
        chat_id=update.effective_chat.id, text=message, parse_mode="MarkdownV2")


def censor_stickers(update, context):
    config = read_config()
    if update.message.sticker.set_name in config["censored_stickers"]:
        context.bot.delete_message(chat_id=update.effective_chat.id,
                                   message_id=update.message.message_id)


def ban_sticker(update, context):
    config = read_config()
    if update.message.reply_to_message:
        try:
            sticker_pack = update.message.reply_to_message.sticker.set_name
        except:
            logging.error("not a sticker!")
            context.bot.send_message(chat_id=update.effective_chat.id,
                                     text=f"Not a sticker\!",
                                     parse_mode="MarkdownV2")
        if sticker_pack not in config["censored_stickers"]:
            config["censored_stickers"].append(sticker_pack)
            update_config(config)
            context.bot.send_message(chat_id=update.effective_chat.id,
                                     text=f"successfully banned `{sticker_pack}`",
                                     parse_mode="MarkdownV2")
        else:
            context.bot.send_message(chat_id=update.effective_chat.id,
                                     text=f"`{sticker_pack}` already banned :\)",
                                     parse_mode="MarkdownV2")

def unban_sticker(update, context):
    config = read_config()
    if update.message.reply_to_message:
        try:
            sticker_pack = update.message.reply_to_message.sticker.set_name
        except:
            logging.error("not a sticker!")
            context.bot.send_message(chat_id=update.effective_chat.id,
                                     text=f"Not a sticker\!",
                                     parse_mode="MarkdownV2")
        else:
            context.bot.send_message(chat_id=update.effective_chat.id,
                                    text=f"successfully unbanned `{sticker_pack}`",
                                    parse_mode="MarkdownV2")
            config["censored_stickers"].remove(sticker_pack)
            update_config(config)


chess_handler = MessageHandler(Filters.regex(r'!chess'), bot_chess)
bigo_handler = MessageHandler(Filters.regex(r'!bigo'), bot_bigo)
commands_handler = MessageHandler(Filters.regex(r'!commands'), bot_commands)
ban_sticker_handler = MessageHandler(Filters.regex(r"!ban"), ban_sticker)
unban_sticker_handler = MessageHandler(Filters.regex(r"!unban"), unban_sticker)

dispatcher.add_handler(CommandHandler("commands", bot_commands))
dispatcher.add_handler(chess_handler)
dispatcher.add_handler(commands_handler)
dispatcher.add_handler(bigo_handler)
dispatcher.add_handler(ban_sticker_handler)
dispatcher.add_handler(unban_sticker_handler)
dispatcher.add_handler(MessageHandler(Filters.sticker, censor_stickers))


updater.start_polling()
